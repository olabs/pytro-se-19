from src.cycles import get_positive_and_negative_sum, get_fibonacci_sequence
from src.cycles import get_arithmetic_mean

from random import random


def test_get_positive_and_negative_sum():
    input_data = {"index": 0, "numbers": [1, -2, 3, -4, 5, 0]}

    def get_next_number() -> int:
        number = input_data["numbers"][input_data["index"]]

        input_data["index"] = min(input_data["index"] + 1,
                                  len(input_data["numbers"]) - 1)

        return number

    sum = get_positive_and_negative_sum(get_next_number)

    assert sum.positive == 9
    assert sum.negative == -6

    # TODO: remove after review
    print(f"\n{input_data['numbers']} -> {sum}\n")


def test_get_fibonacci_sequence():
    fibonacci_sequence = get_fibonacci_sequence(10)

    assert len(fibonacci_sequence) == 10
    assert fibonacci_sequence[4] == 5

    # TODO: remove after review
    print(f"\nfibonacci_sequence(10) -> {fibonacci_sequence}\n")


def test_get_number_by_special_criteria():
    pass


def test_get_arithmetic_mean():
    assert get_arithmetic_mean([5, 25, 125, 5]) == 40
    assert get_arithmetic_mean([]) == 0
    assert get_arithmetic_mean([0]) == 0


def test_get_arithmetic_mean_by_random():
    random_numbers = [random() for x in range(10)]

    assert 0 <= get_arithmetic_mean(random_numbers) < sum(random_numbers)


def test_get_armstrong_numbers():
    pass


def test_get_automorphic_numbers():
    pass
