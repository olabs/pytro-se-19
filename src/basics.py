from src.utils.numbers import get_digits_counts
"""
The problem with these tasks is using input function
- maybe this is a requirement or maybe not
"""


def get_some_strange_stuff():
    x = int(input())
    y = int(input())

    return [
        str.format("{0}+{1}={2}", x, y, x + y),
        str.format("{0} | {1} | {2}", x, y, x + y),
        str.format("Z({0})=F({1})", x, y),
        str.format("x={0}; y={1}", x, y),
        str.format("Ответ: ({0};{1})", x, y)
    ]


def get_number_representation_stuff():
    x = int(input())
    y = int(input())

    return [
        str.format("+ : {0}", x + y),
        str.format("- : {0}", x - y),
        str.format("/ : {0}", x / y),
        str.format("^ : {0}", x**2),
        str.format("bin of x : {0}", bin(x)),
        str.format("octal of x : {0}", oct(x)),
        str.format("hex of x : {0}", hex(x))
    ]


def get_money() -> int:
    print("How many hours do you work?")
    work_hours = int(input())

    print("What is your payment per hour?")
    money_per_hour = int(input())

    pay_for_work = work_hours * money_per_hour

    if work_hours > 40:
        pay_for_work *= 1.5

    return pay_for_work


def has_four_digits() -> bool:
    return get_digits_counts(int(input())) == 4
