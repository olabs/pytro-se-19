from typing import Callable, Optional, List

from collections import namedtuple

from src.utils.numbers import get_digit, get_digits_count, get_digits

Sum = namedtuple('Sum', ['positive', 'negative'])


def get_positive_and_negative_sum(get_next_number: Callable[[], int]) -> Sum:
    sum = Sum(0, 0)

    number_to_add = None

    while True:
        number_to_add = get_next_number()

        if number_to_add == 0:
            break

        if number_to_add > 0:
            sum = Sum(sum.positive + number_to_add, sum.negative)
        else:
            sum = Sum(sum.positive, sum.negative + number_to_add)

    return sum


def get_fibonacci_sequence(number_index: int) -> List[int]:
    fibonacci_numbers = [1, 1]

    if number_index < 2:
        return fibonacci_numbers[0:number_index]

    while number_index > 2:
        fibonacci_numbers.append(fibonacci_numbers[-1] + fibonacci_numbers[-2])
        number_index -= 1

    return fibonacci_numbers


def get_number_by_special_criteria(numbers: List[int]) -> Optional[int]:
    minimal_number = None

    for number_to_check in numbers:
        if number_to_check % 2 != 0 or number_to_check % 3 != 0:
            continue

        if minimal_number is None:
            minimal_number = number_to_check
        elif number_to_check < minimal_number:
            minimal_number = number_to_check

    minimal_number


def get_arithmetic_mean(numbers: List[int]) -> int:
    return sum(numbers, 0) / (len(numbers) if len(numbers) != 0 else 1)


def get_armstrong_numbers(a: int, b: int):
    """
    return all armstrong numbers contained in range [a,b]
    """
    def is_armstrong_number(number: int) -> bool:
        def get_armstrong_number_part(index: int, digits_count: int) -> int:
            return get_digit(number, index, digits_count)**digits_count

        digits_count = get_digits_count(number)

        return sum(
            map(lambda index: get_armstrong_number_part(index, digits_count),
                range(1, digits_count + 1)), 0)

    return list(filter(is_armstrong_number, range(a, b)))


def get_automorphic_numbers(upper_bound: int) -> List[int]:
    def is_automorphic_number(number: int) -> bool:
        squared_number = number**2
        squared_number_length = get_digits_count(squared_number)

        return number == get_digits(
            squared_number, squared_number_length - get_digits_count(number),
            squared_number_length)

    return list(filter(is_automorphic_number, range(0, upper_bound)))


WeekDay = namedtuple('WeekDay', ['name', 'number', 'isWorking'])


def get_week_days() -> List[WeekDay]:
    return [
        WeekDay('Sunday', 1, False),
        WeekDay('Monday', 2, True),
        WeekDay('Tuesday', 3, True),
        WeekDay('Wednesday', 4, True),
        WeekDay('Thursday', 5, True),
        WeekDay('Friday', 6, True),
        WeekDay('Saturday', 7, False)
    ]
