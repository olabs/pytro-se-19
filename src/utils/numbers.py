from typing import Optional


def get_digits_count(number: int) -> int:
    rest_digits = number
    digits_count = 0

    while rest_digits is not None:
        digits_count += 1

        rest_digits //= 10

        if rest_digits == 0:
            rest_digits = None

    return digits_count


def get_digit(number: int, index: int,
              digits_count: int = None) -> Optional[int]:

    if digits_count is None:
        digits_count = get_digits_count(number)

    if digits_count < index:
        return None

    return number // 10**(digits_count - index) % 10


def get_digits(number: int, a: int, b: int) -> Optional[int]:
    """
    return digits with index contained in range [a, b]
    """
    if a <= b:
        return None

    digits_count = get_digits_count(number)

    return number // 10**(digits_count - b) % 10**(b - a)
